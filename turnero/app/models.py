from django.db import models


# Create your models here.
class Hospital(models.Model):
    idhospital = models.AutoField(primary_key=True)
    nombre_hospital = models.CharField(max_length=255)
    direccion = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    telefono = models.CharField(max_length=25)

    class Meta:
        db_table = 'hospital'
class Persona(models.Model):
    idpersona = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255)
    email = models.CharField(max_length=255, blank=True, null=True)
    telefono = models.CharField(max_length=25, blank=True, null=True)
    fecha_nacimiento = models.DateField()
    cedula = models.IntegerField()
    sexo = models.CharField(max_length=12)

    class Meta:
        db_table = 'persona'

class Departamento(models.Model):
    iddepartamento = models.AutoField(primary_key=True)
    idhospital = models.ForeignKey('Hospital', on_delete=models.CASCADE, db_column='idhospital')
    nombre_departamento = models.CharField(max_length=255)

    class Meta:
        db_table = 'departamento'
        unique_together = (('iddepartamento', 'idhospital'),)

class Empleado(models.Model):
    idempleado = models.AutoField(primary_key=True)
    idpersona = models.OneToOneField('Persona',on_delete=models.CASCADE,db_column='idpersona')
    iddepartamento = models.ForeignKey('Departamento',on_delete=models.CASCADE,db_column='iddepartamento')
    codigo_empleado = models.CharField(max_length=255)
    estado = models.CharField(max_length=12,null=True)
    ingreso = models.DecimalField(max_digits=7, decimal_places=2)
    fecha_alta = models.DateTimeField()
    oficio = models.CharField(max_length=255)
    comision = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)

    class Meta:
        db_table = 'empleado'
        unique_together = (('idempleado', 'idpersona', 'iddepartamento'),)

class Rol(models.Model):
    idrol = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=255)

    class Meta:
        db_table = 'rol'

class Sala(models.Model):
    idsala = models.AutoField(primary_key=True)
    iddepartamento = models.ForeignKey('Departamento',on_delete=models.CASCADE,db_column='iddepartamento')
    idhospital = models.ForeignKey('Hospital',on_delete=models.CASCADE,db_column='idhospital')
    nombre = models.CharField(max_length=255)
    numero_cama = models.IntegerField()

class Ticket(models.Model):
    idticket = models.AutoField(primary_key=True)
    numero = models.IntegerField()
    prioridad = models.CharField(max_length=255)
    estado = models.CharField(max_length=20)
    condicion = models.CharField(max_length=255)

    class Meta:
        db_table = 'ticket'

class Servicio(models.Model):
    idservicio = models.AutoField(primary_key=True)
    idticket = models.ForeignKey('Ticket',on_delete=models.CASCADE,db_column='idticket')
    nombre_servicio = models.CharField(max_length=255)

    class Meta:
        db_table = 'servicio'
        unique_together = (('idservicio', 'idticket'),)
class Usuario(models.Model):
    idusuario = models.AutoField(primary_key=True)
    idpersona = models.OneToOneField('Persona',on_delete=models.CASCADE,db_column='idpersona')
    idrol = models.ForeignKey('Rol',on_delete=models.CASCADE,db_column='idrol')
    idservicio = models.ForeignKey('Servicio',on_delete=models.CASCADE,db_column='idservicio')
    password = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    estado = models.CharField(max_length=20)
    fecha_inicio = models.DateTimeField()

    class Meta:
        db_table = 'usuario'
        unique_together = (('idusuario', 'idpersona', 'idrol', 'idservicio'),)

class Plantilla(models.Model):
    idplantilla = models.AutoField(primary_key=True)
    idsala = models.ForeignKey('Sala',on_delete=models.CASCADE,db_column='idsala')
    iddepartamento = models.ForeignKey('Departamento',on_delete=models.CASCADE,db_column='iddepartamento')
    idhospital = models.ForeignKey('Hospital',on_delete=models.CASCADE,db_column='idhospital')
    nombre_paciente = models.CharField(max_length=255)
    cedula = models.IntegerField()
    codigo_empleado = models.IntegerField()

    class Meta:
        db_table = 'plantilla'
        unique_together = (('idplantilla', 'idsala', 'iddepartamento', 'idhospital'),)

class Cola(models.Model):
    idcola = models.AutoField(primary_key=True)
    idticket = models.ForeignKey('Ticket',on_delete=models.CASCADE,db_column='idticket')
    numero_orden = models.IntegerField()
    cedula = models.IntegerField()

    class Meta:
        db_table = 'cola'
        unique_together = (('idcola', 'idticket'),)
