from django.contrib import admin
from . import models
# Register your models here.
admin.site.register(models.Hospital)
admin.site.register(models.Persona)
admin.site.register(models.Departamento)
admin.site.register(models.Empleado)
admin.site.register(models.Rol)
admin.site.register(models.Sala)
admin.site.register(models.Ticket)
admin.site.register(models.Servicio)
admin.site.register(models.Usuario)
admin.site.register(models.Plantilla)
admin.site.register(models.Cola)
